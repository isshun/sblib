package org.smallbox.lib.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;

public abstract class BaseDialogFragment extends DialogFragment {
	private OnCloseListener	mOnCloseListener;
	protected boolean 		mHasBeenActivated;
	private int				mTitle = -1;
	private int 			mNegativeLabel = -1;
	private int 			mPositiveLabel = -1;
	private View 			mView;
	private boolean 		mIsConfirm;
	private boolean 		mIsClose;
    private boolean         mPositiveButtonDisabled;

    protected abstract void onCreateDialog();
	protected abstract void onCancel();
	protected abstract void onConfirm();

	public static interface OnCloseListener {
		void onClose(boolean isConfirm);
	}

	public BaseDialogFragment setOnCloseListener(OnCloseListener onCloseListener) {
    	mOnCloseListener = onCloseListener;
        return this;
	}

    public void show(FragmentManager manager) {
        super.show(manager, "dialog");

        if (mPositiveButtonDisabled) {
            (new Handler(Looper.getMainLooper())).post(new Runnable() {
                @Override
                public void run() {
                    ((AlertDialog)getDialog()).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                }
            });
        }
    }

    public void togglePositiveButtonEnabled(boolean isEnabled) {
        ((AlertDialog)getDialog()).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(isEnabled);
    }

    public void setTitle(int title) {
		mTitle = title;
	}

	public void setNegativeButton(int negativeLabel) {
		mNegativeLabel = negativeLabel;
	}

	public void setPositiveButton(int positiveLabel) {
		setPositiveButton(positiveLabel, true);
	}

	public void setPositiveButton(int positiveLabel, boolean isEnabled) {
		mPositiveLabel = positiveLabel;
	}

    public BaseDialogFragment setPositiveButtonDisabled(boolean positiveButtonDisabled) {
        mPositiveButtonDisabled = positiveButtonDisabled;
        return this;
    }

    @SuppressLint("InflateParams")
	protected void setContentView(int resId) {
		mView = LayoutInflater.from(getActivity()).inflate(resId, null);
	}
	
	protected void setContentView(View view) {
		mView = view;
	}
	
	protected View findViewById(int resId) {
		if (mView == null) {
			throw new RuntimeException("setContentView not called");
		}
		return mView.findViewById(resId);
	}
	
	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
		onCreateDialog();
		
		if (mView == null) {
			throw new RuntimeException("setContentView not called");
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		// Title
		if (mTitle != -1) {
			builder.setTitle(mTitle);
		}
		
		// Content view
		if (mView != null) {
            builder.setView(mView);
		}

		// Positive button
		if (mPositiveLabel != -1) {
            builder.setPositiveButton(mPositiveLabel,
            		new DialogInterface.OnClickListener() {
                    	public void onClick(DialogInterface dialog, int whichButton) {
                    		mIsConfirm = true;
                    		onConfirm();
                    		if (!mIsClose && mOnCloseListener != null) {
                    			mIsClose = true;
                    			mOnCloseListener.onClose(true);
                    		}
                    	}
                	}
            );
		}
		
		// Negative button
		if (mNegativeLabel != -1) {
            builder.setNegativeButton(mNegativeLabel, 
            		new DialogInterface.OnClickListener() {
            			public void onClick(DialogInterface dialog, int whichButton) {
            				onCancel();
                    		if (!mIsClose && mOnCloseListener != null) {
                    			mIsClose = true;
                    			mOnCloseListener.onClose(false);
                    		}
            			}
            		}
            );
		}

		AlertDialog dialog = builder.create();
		return dialog;
    }

	protected void save() {
		mIsConfirm = true;
		onConfirm();
		if (!mIsClose && mOnCloseListener != null) {
			mIsClose = true;
			mOnCloseListener.onClose(true);
		}
		dismiss();
	}

	public boolean isConfirm() {
		return mIsConfirm;
	}
	
	protected void cancel() {
		onCancel();
		dismiss();
	}

	public boolean hasBeenActivated() {
		return mHasBeenActivated;
	}
	
	@Override
	public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
		if (!mIsClose && mOnCloseListener != null) {
			mIsClose = true;
			mOnCloseListener.onClose(false);
		}
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		if (!mIsClose && mOnCloseListener != null) {
			mIsClose = true;
			mOnCloseListener.onClose(false);
		}
	}
}
