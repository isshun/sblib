package org.smallbox.lib;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import org.smallbox.lib.font.FontManager;
import org.smallbox.lib.utils.Utils;
import org.smallbox.lib.volley.BitmapLruCache;

/**
 * Created by Alex on 21/04/2015.
 */
public abstract class SBApplication extends android.app.Application {
    private static Context 		            sContext;
    private static int			            sScreenWidth;
    private static int			            sScreenHeight;
    public static boolean                   sHasSmallScreen;

    private static RequestQueue             sVolleyRequestQueue;
    private static ImageLoader 	            sVolleyImageLoader;
    private static ImageLoader              sVolleyLocalImageLoader;
    private static boolean                  sDebug;

    private static FontManager              sFontManager;
    private static RouteManager             sRouteManager;

    public static Context getContext() { return sContext; }
    public static int getScreenWidth() { return sScreenWidth; }
    public static int getScreenHeight() { return sScreenHeight; }
    public static int getDimension(int resId) { return (int)sContext.getResources().getDimension(resId); }
    public static ImageLoader getVolleyImageLoader() { return sVolleyImageLoader; }
    public static ImageLoader getVolleyLocalImageLoader() { return sVolleyLocalImageLoader; }
    public static RequestQueue getVolleyRequestQueue() { return sVolleyRequestQueue; }

    @Override
    public void onCreate () {
        super.onCreate();
        sContext = getApplicationContext();
        SmallboxLib.setContext(sContext);

        // Volley
        sVolleyRequestQueue = Volley.newRequestQueue(getApplicationContext());
        sVolleyImageLoader = new ImageLoader(sVolleyRequestQueue, new BitmapLruCache());
        sVolleyLocalImageLoader = new ImageLoader(sVolleyRequestQueue, new ImageLoader.ImageCache() {
            @Override
            public Bitmap getBitmap(String key) {
                return BitmapFactory.decodeFile(key.substring(key.indexOf("/")));
            }

            @Override
            public void putBitmap(String key, Bitmap bitmap) {}
        });
        sVolleyRequestQueue.start();

        WindowManager wm = (WindowManager)getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        sScreenWidth = Math.min(size.x, size.y);
        sScreenHeight = Math.max(size.x, size.y);

        sHasSmallScreen = Utils.convertPixelsToDp(sScreenHeight) < 520;

        sFontManager = new FontManager(sContext);
        onLoadFonts(sFontManager);

        sRouteManager = new RouteManager();
        onInitRoutes(sRouteManager);
        sRouteManager.init(sVolleyRequestQueue);
    }

    public void setDebug(boolean isDebug) {
        sDebug = isDebug;
    }

    protected abstract void onInitRoutes(RouteManager manager);

    @Override
    public void onTerminate() {
        sVolleyRequestQueue.stop();
        super.onTerminate();
    }

    public abstract void onLoadFonts(FontManager manager);

    public static boolean isDebug() {
        return sDebug;
    }

    public static RouteManager getRouteManager() {
        return sRouteManager;
    }

    public static FontManager getFontManager() {
        return sFontManager;
    }
}
