package org.smallbox.lib.utils;

import android.content.ContextWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.smallbox.lib.SmallboxLib;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FileUtils {
    private static final ContextWrapper CONTEXT_WRAPPER = new ContextWrapper(SmallboxLib.getContext());

    private static final int BUFFER_SIZE = 4096;
    protected static final byte[] BUFFER = new byte[BUFFER_SIZE];
    private static final char[] BUFFER_CHAR = new char[BUFFER_SIZE];

    public static String getMD5(File file) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            MessageDigest md = MessageDigest.getInstance("MD5");

            //Using MessageDigest send() method to provide input
            int numOfBytesRead;
            while( (numOfBytesRead = fis.read(BUFFER)) > 0){
                md.update(BUFFER, 0, numOfBytesRead);
            }
            byte[] hash = md.digest();
            return new BigInteger(1, hash).toString(16); //don't use this, truncates leading zero
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } finally {
            if (fis != null) {
                try { fis.close(); }
                catch (IOException e) { e.printStackTrace(); }
            }
        }

        return null;
    }

    public static boolean exists(String filename) {
        return getFile(filename).exists();
    }

    public static File getDirectory() {
        return getDirectory(null);
    }

    public static File getFile(String filename) {
        return new File(getDirectory(), filename);
    }

    public static File getFile(String directoryName, String filename) {
        return new File(getDirectory(directoryName), filename);
    }

    public static File getDirectory(String directoryName) {
        return directoryName != null ? new File(CONTEXT_WRAPPER.getFilesDir(), directoryName) : CONTEXT_WRAPPER.getFilesDir();
    }

    public static void unzipFromAsset(String filename) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = SmallboxLib.getContext().getAssets().open(filename);
            File outFile = new File(getDirectory(), "tmp.zip");
            out = new FileOutputStream(outFile);
            int read;
            while((read = in.read(BUFFER)) != -1){
                out.write(BUFFER, 0, read);
            }
            ZipUtils.unpackZip(getDirectory(), outFile);
            outFile.delete();
        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) { try { in.close(); } catch (IOException e) {} }
            if (out != null) { try { out.close(); } catch (IOException e) {} }
        }
    }

    public static String readFromAsset(String filename) {
        try {
            StringBuilder buf = new StringBuilder();
            InputStream is = SmallboxLib.getContext().getAssets().open(filename);
            BufferedReader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));

            String str;
            while ((str = in.readLine()) != null) {
                buf.append(str);
            }

            in.close();
            is.close();

            return buf.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String read(String filename) {
        return read(filename, false);
    }

    public static String read(String filename, boolean isFromAsset) {
        return read(getBufferedReader(filename, isFromAsset));
    }

    public static String read(File file) {
        return read(getBufferedReader(file));
    }

    private static String read(BufferedReader br) {
        if (br != null) {
            try {
                StringBuilder sb = new StringBuilder();
                int length;
                while ((length = br.read(BUFFER_CHAR, 0, BUFFER_SIZE)) > 0) {
                    sb.append(BUFFER_CHAR, 0, length);
                }
                br.close();
                return sb.toString();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static void write(String filename, String json) {
        try {
            ContextWrapper c = new ContextWrapper(SmallboxLib.getContext());
            FileOutputStream fos = new FileOutputStream(new File(c.getFilesDir(), filename));
            fos.write(json.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static BufferedReader getBufferedReader(String filename, boolean fromAssets) {
        // Read from assets
        if (fromAssets) {
            try {
                InputStream is = SmallboxLib.getContext().getAssets().open(filename);
                return new BufferedReader(new InputStreamReader(is, "UTF-8"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Read from files
        else {
            return getBufferedReader(new File(getDirectory(), filename));
        }

        return null;
    }

    private static BufferedReader getBufferedReader(File file) {
        try {
            return new BufferedReader(new FileReader(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static boolean has(String filename) {
        ContextWrapper c = new ContextWrapper(SmallboxLib.getContext());
        return (new File(c.getFilesDir(), filename)).exists();
    }
}
