package org.smallbox.lib.utils;

import android.content.ContextWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.smallbox.lib.SmallboxLib;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public abstract class JSONUtilsBase {
	private static Map<String, String>	sHashs = new HashMap<String, String>();

	public static boolean isUpToDate(String filename, String hashToCompare) {
		String localHash = sHashs.get(filename);
		return (localHash != null && localHash.equals(hashToCompare));
	}

    public static void write(String filename, JSONObject json) {
        try {
            write(filename, json.toString(4));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void write(String filename, JSONArray json) {
        try {
            write(filename, json.toString(4));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static String getMD5(String filename) {
		return sHashs.get(filename);
	}

    private static void write(String filename, String json) {
        FileUtils.write(filename, json);
        createMD5(filename, json);
    }

    private static void createMD5(String filename, String json) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			InputStream is = new ByteArrayInputStream(json.getBytes(Charset.forName("UTF-8")));
			DigestInputStream dis = new DigestInputStream(is, md);
			StringBuilder sb = new StringBuilder();
            for (byte b : md.digest()) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
            }
			dis.close();
			sHashs.put(filename, sb.toString());
		} catch (IOException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
    }

	public static JSONArray loadArray(String filename, boolean fromAssets) {
		String json = FileUtils.read(filename, fromAssets);
		if (json != null) {
			try {
				return new JSONArray(json);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static JSONObject loadObject(String filename) {
		return loadObject(filename, false);
	}

	public static JSONObject loadObject(String filename, boolean fromAssets) {
		String json = FileUtils.read(filename, fromAssets);
		if (json != null) {
			try {
				return new JSONObject(json);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static void copyFromAssets(String from, String to) {
		try {
			ContextWrapper c = new ContextWrapper(SmallboxLib.getContext());
			InputStream is = SmallboxLib.getContext().getAssets().open(from);
			FileOutputStream fos = new FileOutputStream(new File(c.getFilesDir(), to));

			int len = 0;
			byte[] buffer = new byte[512];
			while ((len = is.read(buffer)) > 0) {
				fos.write(buffer, 0, len);
			}

			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void remove(String filename) {
		ContextWrapper c = new ContextWrapper(SmallboxLib.getContext());
		File file = new File(c.getFilesDir(), filename);
		if (file.exists()) {
			file.delete();
		}
	}

	public static JSONArray loadArray(String filename) {
		return loadArray(filename, false);
	}

}
