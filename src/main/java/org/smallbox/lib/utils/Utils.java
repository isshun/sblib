package org.smallbox.lib.utils;

import android.content.res.Resources;
import android.util.DisplayMetrics;

import org.smallbox.lib.SmallboxLib;

import java.util.Calendar;

public class Utils {

    public static boolean isSameDay(Calendar c1, Calendar c2) {
        return c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR);
    }

    public static boolean isToday(Calendar calendar) {
        return isSameDay(Calendar.getInstance(), calendar);
    }

    public static boolean isTomorrow(Calendar calendar) {
        Calendar tomorrow = Calendar.getInstance();
        tomorrow.add(Calendar.DAY_OF_YEAR, 1);
        return isSameDay(tomorrow, calendar);
    }


    public static int convertDpToPixel(float dp) {
        Resources resources = SmallboxLib.getContext().getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = (int)(dp * (metrics.densityDpi / 160f));
        return px;
    }

    public static float convertPixelsToDp(float px) {
        Resources resources = SmallboxLib.getContext().getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public static String capitalize(String str) {
        return str;
    }
}
