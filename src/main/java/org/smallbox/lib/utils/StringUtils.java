package org.smallbox.lib.utils;

import java.util.Date;

public class StringUtils {

	public static String getFormatGrade(float value) {
        if (value < 0) {
            return "--";
        } else if ((int)(value * 10) % 10 > 0) {
			return String.valueOf((int)(value * 10) / 10f);
		} else {
			return String.valueOf((int)(value));
		}
	}

	public static String toRomanNumber(int number) {
		switch (number) {
		case 1: return "I";
		case 2: return "II";
		case 3: return "III";
		case 4: return "IV";
		case 5: return "V";
		case 6: return "VI";
		case 7: return "VII";
		case 8: return "VIII";
		case 9: return "IX";
		case 10: return "X";
		case 11: return "XI";
		case 12: return "XII";
		case 13: return "XIII";
		case 14: return "XIV";
		case 15: return "XV";
		case 16: return "XVI";
		case 17: return "XVII";
		case 18: return "XVIII";
		case 19: return "XIX";
		case 20: return "XX";
		case 21: return "XXI";
		case 22: return "XXII";
		case 23: return "XXIII";
		case 24: return "XXIV";
		case 25: return "XXV";
		case 26: return "XXVI";
		case 27: return "XXVII";
		case 28: return "XXVIII";
		case 29: return "XXIX";
		case 30: return "XXX";
		}

		return "";
	}

	public static String getFormattedPhoneNumber(String phone) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < phone.length(); i++) {
			if (phone.charAt(i) >= '0' && phone.charAt(i) <= '9') {
				sb.append(phone.charAt(i));
			}
		}

		if (sb.length() >= 10) {
			sb.replace(0, sb.length() - 9, "");
			sb.insert(0, "+33 ");
			sb.insert(5, " ");
			sb.insert(8, " ");
			sb.insert(11, " ");
			sb.insert(14, " ");
		}

		return sb.toString();
	}

    public static int compareDate(Date date1, Date date2) {
        if (date1 == null && date2 == null) return 0;
        if (date1 == null) return -1;
        if (date2 == null) return 1;
        return date1.compareTo(date2);
    }

    public static String toPrefixNumber(int value) {
        switch (value) {
            case 0: return "";
            case 1: return "1er";
            case 2: return "2nd";
            default: return value + "ème";
        }
    }

    public static String capitalize(String string) {
        if (string.length() > 2) {
            return Character.toUpperCase(string.charAt(0)) + string.substring(1);
        } else if (string.length() > 1) {
            return string.toUpperCase();
        } else {
            return string;
        }
    }

    public static String capitalizeFully(String str, final char... delimiters) {
        final int delimLen = delimiters == null ? -1 : delimiters.length;
        if (str == null || str.isEmpty() || delimLen == 0) {
            return str;
        }
        str = str.toLowerCase();
        return capitalize(str, delimiters);
    }

    public static String capitalize(String str, final char... delimiters) {
        final int delimLen = delimiters == null ? -1 : delimiters.length;
        if (str == null || str.isEmpty() || delimLen == 0) {
            return str;
        }
        final char[] buffer = str.toCharArray();
        boolean capitalizeNext = true;
        for (int i = 0; i < buffer.length; i++) {
            final char ch = buffer[i];
            if (isDelimiter(ch, delimiters)) {
                capitalizeNext = true;
            } else if (capitalizeNext) {
                buffer[i] = Character.toTitleCase(ch);
                capitalizeNext = false;
            }
        }
        return new String(buffer);
    }

    private static boolean isDelimiter(final char ch, final char[] delimiters) {
        if (delimiters == null) {
            return Character.isWhitespace(ch);
        }
        for (final char delimiter : delimiters) {
            if (ch == delimiter) {
                return true;
            }
        }
        return false;
    }

}
