package org.smallbox.lib.utils;

import android.content.ContextWrapper;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import org.smallbox.lib.SmallboxLib;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipUtils {
    private static final int BUFFER = 2048;

    public static class ZipExportException extends RuntimeException {
        public ZipExportException(String message) {
            super(message);
        }
    }

    public static File export() {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            // Get zip file
            File externalDirectory = Environment.getExternalStoragePublicDirectory(Build.VERSION.SDK_INT >= 19 ? Environment.DIRECTORY_DOCUMENTS : Environment.DIRECTORY_DOWNLOADS);
            externalDirectory.mkdir();
            if (externalDirectory == null) { throw new ZipExportException("Auncun support d'enregistrement disponible"); }
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_hh_mm", Locale.FRANCE);
            File zipFile = new File(externalDirectory, "suivi_de_classes_" + formatter.format(new Date()) + ".zip");

            // Get files to exports
            ContextWrapper c = new ContextWrapper(SmallboxLib.getContext());
            File[] fileToExport = c.getFilesDir().listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File file, String filename) {
                    return filename.endsWith(".json") || filename.equals("data.db");
                }
            });

            File[] sessionFileToExport = new File(c.getFilesDir(), "sessions").listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File file, String filename) {
                    return filename.endsWith(".json");
                }
            });

            // Zip
            try  {
                BufferedInputStream origin = null;
                FileOutputStream dest = new FileOutputStream(zipFile);

                ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));

                byte data[] = new byte[BUFFER];

                // Add database
                addFile(origin, out, data, c.getDatabasePath("data.db"), "");

                for (int i = 0; i < fileToExport.length; i++) {
                    addFile(origin, out, data, fileToExport[i], "");
                }

                for (int i = 0; i < sessionFileToExport.length; i++) {
                    addFile(origin, out, data, sessionFileToExport[i], "sessions/");
                }

                out.close();

                return zipFile;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            throw new ZipExportException(Environment.getExternalStorageState());
        }
        return null;
    }

    private static void addFile(BufferedInputStream origin, ZipOutputStream out, byte[] data, File file, String path) {
        try {
            Log.v("[ZipUtils]", "Adding: " + file.getName());
            FileInputStream fi = new FileInputStream(file);
            origin = new BufferedInputStream(fi, BUFFER);
            ZipEntry entry = new ZipEntry(path + file.getName());
            out.putNextEntry(entry);
            int count;
            while ((count = origin.read(data, 0, BUFFER)) != -1) {
                out.write(data, 0, count);
            }
            origin.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean unpackZip(File directory, File zipFile) {
        InputStream is;
        ZipInputStream zis;

        try {
            String filename;
            is = new FileInputStream(zipFile);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;
            byte[] buffer = new byte[4096];
            int count;
            int nbFiles = 0;

            while ((ze = zis.getNextEntry()) != null) {
                filename = ze.getName();

                // Need to create directories if not exists, or
                // it will generate an Exception...
                if (ze.isDirectory()) {
                    File fmd = new File(directory, filename);
                    fmd.mkdirs();
                    continue;
                }

//                Log.v("", "unzip " + filename + " to " + new File(directory, filename));

                FileOutputStream fOut = new FileOutputStream(new File(directory, filename));

                while ((count = zis.read(buffer)) != -1) {
                    fOut.write(buffer, 0, count);
                }

                fOut.close();
                zis.closeEntry();
                nbFiles++;
            }

            Log.d("[ZipUtils]", "unzip " + nbFiles + " files to " + directory.getAbsolutePath());

            zis.close();
        } catch(IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

}
