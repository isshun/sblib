package org.smallbox.lib.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import org.smallbox.lib.RouteManager;
import org.smallbox.lib.SBApplication;

/**
 * Created by Alex on 21/04/2015.
 */
public class SBFragment extends Fragment {
    protected RouteManager mRouteManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRouteManager = SBApplication.getRouteManager();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
