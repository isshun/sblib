package org.smallbox.lib.datasource;

import android.database.sqlite.SQLiteDatabase;

import java.util.Collection;
import java.util.List;

public interface DataManagerInterface<T_ITEM extends SBCollection.ItemModel, T_ID> {
	void 	        save(T_ITEM object);
	void 	        saveAll();
    List<T_ITEM>    getAll();
	T_ITEM          get(T_ID id);
	void	        add(T_ITEM object);
	void	        add(T_ITEM object, SQLiteDatabase db);
	void	        addAll(List<T_ITEM> object);
	void	        addAll(List<T_ITEM> object, boolean insertToDB);
	void	        truncate();
	void	        truncate(String tableName);
	void	        remove(T_ITEM object);
	void	        remove(T_ITEM object, SQLiteDatabase db);
    void            removeAll(Collection<T_ITEM> toRemove);
}
