package org.smallbox.lib.datasource;

import android.database.sqlite.SQLiteDatabase;

import org.smallbox.lib.datasource.sql.SQLTableManagerBase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SBCollection<T extends SBCollection.ItemModel, T_ID> {

    private SQLTableManagerBase<T, T_ID> mDBTable;

    public void save(T item) {
        if (mDBTable != null) {
            mDBTable.save(item);
        }
    }

    public void setDBTable(SQLTableManagerBase<T, T_ID> dBTable) {
        mDBTable = dBTable;
    }

    public interface UpdateStrategy<T> {
        void onUpdate(T existing, T adding);
    }

    public static abstract class ItemModel<T_ID> {
        private T_ID    mId;
        private long    mDbId;
        private boolean mNeedSave;

        public void setDbId(long dbId) {
            mDbId = dbId;
        }

        public long getDbId() {
            return mDbId;
        }

        public T_ID getId() {
            return mId;
        }

        public void save() {
            mNeedSave = true;
        }

        public boolean needSave() {
            return mNeedSave;
        }

        public void setSaved() {
            mNeedSave = false;
        }
    }

    private List<T>				mList;
    private Map<Object, T>		mMap;
    private Comparator<T> 		mComparator;
    private UpdateStrategy<T>   mUpdateStrategy;
    private boolean 			mListNeedRefresh = true;
    private boolean             mShuffle = false;

    public SBCollection() {
        mList = new ArrayList<>();
        mMap = new HashMap<>();
    }

    public void setShuffle(boolean shuffle) {
        mShuffle = shuffle;
    }

    public List<T> getList() {
        if (mListNeedRefresh) {
            mList.clear();
            mList.addAll(mMap.values());
            if (mComparator != null) {
                Collections.sort(mList, mComparator);
            }
            if (mShuffle) {
                Collections.shuffle(mList);
            }
        }
        return mList;
    }

    public void add(T item, boolean insertToDB) {
        add(item, null, insertToDB);
    }

    public void add(T item, SQLiteDatabase db) {
        add(item, db, true);
    }

    public void add(T item, SQLiteDatabase db, boolean insertToDB) {
        mListNeedRefresh = true;
        T existing = mMap.get(item.getId());
        if (existing != null) {
            item.setDbId(existing.getDbId());
        }
        if (mUpdateStrategy != null && existing != null) {
            mUpdateStrategy.onUpdate(existing, item);
        } else {
            mMap.put(item.getId(), item);
        }
        if (mDBTable != null && insertToDB) {
            if (db != null) mDBTable.add(item, db);
            else mDBTable.add(item);
        }
    }

    public void add(T item) {
        add(item, true);
    }

    public void addAll(List<T> collection) {
        mListNeedRefresh = true;
        for (T item: collection) {
            add(item, false);
        }
        if (mDBTable != null) {
            mDBTable.addAll(collection);
        }
    }

    public boolean has(T_ID key) {
        return mMap.containsKey(key);
    }

    public T get(T_ID name) {
        return mMap.get(name);
    }

    public void setComparator(Comparator<T> comparator) {
        mComparator = comparator;
    }

    public void setUpdateStrategy(UpdateStrategy<T> updateStrategy) { mUpdateStrategy = updateStrategy; }

    public void remove(T data, SQLiteDatabase db, boolean removeFromDB) {
        mListNeedRefresh = true;
        mMap.remove(data.getId());
        if (mDBTable != null && removeFromDB) {
            if (db != null) mDBTable.remove(data, db);
            else mDBTable.remove(data);
        }
    }

    public void remove(T data) {
        remove(data, null, true);
    }

    public void remove(T data, boolean removeFromDB) {
        remove(data, null, removeFromDB);
    }

    public void remove(T data, SQLiteDatabase db) {
        remove(data, db, true);
    }

    public void removeAll(Collection<T> collection) {
        mListNeedRefresh = true;
        for (T data: collection) {
            remove(data, null, false);
        }
        if (mDBTable != null) {
            mDBTable.removeAll(collection);
        }
    }

    public void clear() {
        mListNeedRefresh = true;
        mMap.clear();
        if (mDBTable != null) {
            mDBTable.truncate();
        }
    }

    public int size() {
        return mMap.size();
    }
}
