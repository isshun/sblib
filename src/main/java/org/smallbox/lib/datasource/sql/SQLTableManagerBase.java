package org.smallbox.lib.datasource.sql;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.smallbox.lib.datasource.SBCollection;
import org.smallbox.lib.datasource.DataManagerInterface;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Alex on 20/02/2015.
 */
public abstract class SQLTableManagerBase<T extends SBCollection.ItemModel, T_ID> implements DataManagerInterface<T, T_ID> {
    private final String                    mTableName;
    private final String                    mColumnId;
    protected final SQLManagerBase          mSqlManager;
    protected final SBCollection<T, T_ID>   mDataCollection;

    public SQLTableManagerBase(SQLManagerBase sqlManager, String tableName, String columnId, Comparator<T> comparator, SBCollection<T, T_ID> dataCollection) {
        mSqlManager = sqlManager;
        mDataCollection = dataCollection;
        mDataCollection.setDBTable(this);
        if (comparator != null) {
            mData.setComparator(comparator);
        }
        mTableName = tableName;
        mColumnId = columnId;

        long time = System.currentTimeMillis();
        SQLiteDatabase db = mSqlManager.getWritableDatabase();
        onLoadFromDB(db, mDataCollection);
        db.close();
        Log.i("SectionManagerSQL", "[SectionManagerSQL] load " + mDataCollection.size() + " entries from " + mTableName + " (" + (System.currentTimeMillis() - time) + "ms)");
    }

    protected abstract void onLoadFromDB(SQLiteDatabase db, SBCollection<T, T_ID> data);

    @Override
    public List<T> getAll() {
        return mDataCollection.getList();
    }

    @Override
    public T get(T_ID id) {
        return mDataCollection.get(id);
    }

    /**
     * Add to DB
     *
     * @param model
     */
    public void add(T model) {
        SQLiteDatabase db = mSqlManager.getWritableDatabase();
        add(model, db);
        db.close();
    }

    /**
     * Add to DB
     *
     * @param model
     */
    public void add(T model, SQLiteDatabase db) {
        ContentValues values = getContentValues(model);
        model.setDbId(db.insert(mTableName, null, values));
    }

    /**
     * Add to DB
     *
     * @param modelList
     */
    public void addAll(List<T> modelList) {
        addAll(modelList, true);
    }

    /**
     * Add to DB
     *
     * @param modelList
     */
    public void addAll(List<T> modelList, boolean insertToDB) {
        if (insertToDB) {
            SQLiteDatabase db = mSqlManager.getWritableDatabase();
            for (T model: modelList) {
                ContentValues values = getContentValues(model);
                model.setDbId(db.insert(mTableName, null, values));
            }
            db.close();
        }
    }

    /**
     * Save to DB
     *
     * @param model
     */
    @Override
    public void save(T model) {
        SQLiteDatabase db = mSqlManager.getWritableDatabase();
        ContentValues values = getContentValues(model);
        db.update(mTableName, values, mColumnId + " = ?", new String[]{String.valueOf(model.getDbId())});
        db.close();
    }

    /**
     * Save all data to DB
     */
    @Override
    public void saveAll() {
        throw new RuntimeException("not implemented");
//        SQLiteDatabase db = this.getWritableDatabase();
//        for (T model: mDataCollection.getList()) {
//            if (model.needSave()) {
//                ContentValues values = getContentValues(model);
//                db.update(mTableName, values, mColumnId + " = ?", new String[] { String.valueOf(model.getDbId()) });
//                model.setSaved();
//            }
//        }
//        db.close();
    }

    /**
     * Truncate
     */
    @Override
    public void truncate() {
        truncate(mTableName);
    }

    /**
     * Truncate
     */
    @Override
    public void truncate(String tableName) {
        SQLiteDatabase db = mSqlManager.getWritableDatabase();
        db.delete(tableName, null, null);
        db.close();
    }

    /**
     * Remove from DB
     *
     * @param model
     */
    @Override
    public void remove(T model) {
        SQLiteDatabase db = mSqlManager.getWritableDatabase();
        remove(model, db);
        db.close();
    }

    public void remove(T model, SQLiteDatabase db) {
        db.delete(mTableName, mColumnId + " = ?", new String[]{String.valueOf(model.getDbId())});
    }

    /**
     * Remove from DB
     *
     * @param collection
     */
    @Override
    public void removeAll(Collection<T> collection) {
        SQLiteDatabase db = mSqlManager.getWritableDatabase();
        for (T model: collection) {
            db.delete(mTableName, mColumnId + " = ?", new String[] { String.valueOf(model.getDbId()) });
        }
        db.close();
    }

    protected abstract ContentValues getContentValues(T model);

}
