package org.smallbox.lib.datasource.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Alex on 21/01/2015.
 */
public abstract class SQLManagerBase extends SQLiteOpenHelper {

    public SQLManagerBase(Context context, String name, int version) {
        super(context, name, null, version);

        onLoad(getWritableDatabase());
    }

    protected abstract void onLoad(SQLiteDatabase db);
}
