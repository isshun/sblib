package org.smallbox.lib.font;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alex on 21/04/2015.
 */
public class FontManager {
    private static Map<String, FontModel> sFonts = new HashMap<>();
    private static Map<Integer, FontModel> sFontsByIndex = new HashMap<>();
    private static Typeface sDefaultFont;
    private static Context mContext;

    private static class FontModel {
        public final String             name;
        public final String             path;
        public final int                index;
        public Typeface typeface;

        public FontModel(int index, String name, String path) {
            this.index = index;
            this.name = name;
            this.path = path;
        }
    }

    public FontManager(Context context) {
        mContext = context;
    }

    public void addFont(int index, String name, String path) {
        FontModel font = new FontModel(index, name, path);
        sFonts.put(name, font);
        sFontsByIndex.put(index, font);
    }

    public static void setDefault(Typeface typeface) {
        sDefaultFont = typeface;
    }

    public static Typeface getDefaultFont() {
        return sDefaultFont;
    }

    public static boolean hasDefaultFont() {
        return sDefaultFont != null;
    }

    public static Typeface getFont(int index) {
        if (sFontsByIndex.containsKey(index)) {
            FontModel font = sFontsByIndex.get(index);
            if (font.typeface == null) {
                font.typeface = Typeface.createFromAsset(mContext.getAssets(), font.path);
            }
            return font.typeface;
        }
        return null;
    }

    public static Typeface getFont(String name) {
        if (sFonts.containsKey(name)) {
            FontModel font = sFonts.get(name);
            if (font.typeface == null) {
                font.typeface = Typeface.createFromAsset(mContext.getAssets(), font.path);
            }
            return font.typeface;
        }
        return null;
    }

}
