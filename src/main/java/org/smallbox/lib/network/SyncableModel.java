package org.smallbox.lib.network;

/**
 * Created by Alex on 21/04/2015.
 */
public abstract class SyncableModel {
    public abstract void sync();
}
