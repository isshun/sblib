package org.smallbox.lib.network.cache;

import org.smallbox.lib.utils.FileUtils;

public class RestFileCache implements RestCache {
	private static long			TIME_BETWEEN_REQUEST = 30000;
	
	private String 				mFilename;
//	private String 				mFileHash;
	private long 				mLastCall;
    private String              mData;

    public RestFileCache(String filename) {
		mFilename = filename;
	}
	
	@Override
	public void save(String response) {
        mData = response;
		FileUtils.write(mFilename, response);
//		mFileHash = JSONUtils.getMD5(mFilename);
		mLastCall = System.currentTimeMillis();
	}

	@Override
	public boolean hasData() {
		return FileUtils.has(mFilename);
	}

	@Override
	public boolean isUpToDate() {
		return mLastCall != -1 && System.currentTimeMillis() - mLastCall < TIME_BETWEEN_REQUEST;
	}

	@Override
	public String getFilename() {
		return mFilename;
	}

	@Override
	public void invalidate() {
		mLastCall = -1;
	}

    @Override
    public boolean exists() {
        if (FileUtils.has(mFilename) && mData == null) {
            mData = FileUtils.read(mFilename);
        }
        return mData != null;
    }

    @Override
    public String getData() {
        return mData;
    }

}
