package org.smallbox.lib.network.cache;

public interface RestCache {
	void save(String response);
	boolean hasData();
	boolean isUpToDate();
	String getFilename();
	void invalidate();
    boolean exists();
    String getData();
}
