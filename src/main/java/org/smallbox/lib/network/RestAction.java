package org.smallbox.lib.network;

import android.content.Intent;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;
import org.smallbox.lib.EventManager;
import org.smallbox.lib.RouteManager;
import org.smallbox.lib.SBApplication;
import org.smallbox.lib.activity.RestDebugActivity;
import org.smallbox.lib.network.cache.RestCache;
import org.smallbox.lib.network.factory.IRequestBodyFactory;
import org.smallbox.lib.network.listener.ActionListener;
import org.smallbox.lib.network.request.PostRequest;

import java.nio.charset.Charset;

public class RestAction<T> {
    private String						    mUrl;
    private String                          mActionName;
    private IRequestBodyFactory             mRequestBodyFactory;
    private boolean 					    mIsRunning;
    private ActionListener                  mActionListener;
    private RestCache                       mCache;
    private EventManager.OnEventListener    mEventListener;
    private RouteManager                    mManager;

    public RestAction(String actionName, String url, RestCache cache, IRequestBodyFactory requestBodyFactory, ActionListener actionListener) {
        mActionListener = actionListener;
        mActionName = actionName;
        mUrl = url;
        mCache = cache;
        mRequestBodyFactory = requestBodyFactory;
    }

    public void execute(EventManager.OnEventListener eventListener) {
        Log.i("RestService", "["+mActionName+"] Send request");

        mEventListener = eventListener;

        // Cached data exists
        if (mCache != null && mCache.hasData()) {

            // Is up to date
            if (mCache.isUpToDate()) {
                Log.i("RestService", "["+mActionName+"] Get cache");
                getCache();
            }

            // Need to refresh
            else {
                Log.i("RestService", "["+mActionName+"] Get cache + get live");
                getCache();
                getLive();
            }
        }

        // No cached data
        else {
            Log.i("RestService", "["+mActionName+"] Get live");
            getLive();
        }
    }

    protected void getLive() {
        Request<String> req = createRequest(

                // Success
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("RestService", "["+mActionName+"] Success");

                        if (mActionListener != null) {
                            mActionListener.onReceive(response);
                        }

                        // Write cache
                        if (mCache != null) {
                            mCache.save(response);
                        }

                        mIsRunning = false;

                        if (mEventListener != null) {
                            mEventListener.onEvent(true);
                        }
                    }
                },

                // Failure
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("RestClient", "["+mActionName+"] Error: " + error.getMessage());
                        mIsRunning = false;

                        if (SBApplication.isDebug()) {
//                            String message = new String(error.networkResponse.data, Charset.forName("UTF-8"));
//                            Intent intent = new Intent(SBApplication.getContext(), RestDebugActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            intent.putExtra("data", message);
//                            SBApplication.getContext().startActivity(intent);
                        }

                        if (mEventListener != null) {
                            mEventListener.onFailure(error.getMessage());
                        }
                    }
                });

        if (req != null) {
            mManager.addToQueue(req, false);
            mIsRunning = true;
        }
    }

    private Request<String> createRequest(Response.Listener<String> successListener, Response.ErrorListener errorListener) {
        // Post
        if (mRequestBodyFactory != null) {
            JSONObject obj = mRequestBodyFactory.toJSON();
            if (obj != null) {
                String body = obj.toString();
                Log.d("RestAction", "[RestAction] Body: " + body);
                return new PostRequest(mUrl, body, successListener, errorListener);
            } else {
                Log.w("RestService", "[RestAction] Cannot create POST body, request canceled");
            }
        }

        // Get
        else {
            return new StringRequest(mUrl, successListener, errorListener);
        }

        return null;
    }

    protected void getCache() {
        Log.d("RestService", "["+mActionName+"] Return cached data");

        if (mCache.exists()) {
            mActionListener.onReadCache(mCache.getData());

            if (mEventListener != null) {
                mEventListener.onEvent(false);
            }
        }
    }

    public boolean isRunning() {
        return mIsRunning;
    }

    public String getActionName() {
        return mActionName;
    }

    public void cancel() {
        mIsRunning = false;
        if (mCache != null) {
            mCache.invalidate();
        }
    }

    public EventManager.OnEventListener getListener() {
        return mEventListener;
    }

    public void removeListener() {
        mEventListener = null;
    }

    public void setManager(RouteManager manager) {
        mManager = manager;
    }
}
