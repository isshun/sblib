package org.smallbox.lib.network.factory;

import org.json.JSONObject;

public interface IRequestBodyFactory {
	JSONObject toJSON();
}
