package org.smallbox.lib.network.factory;

import org.json.JSONObject;

public abstract class IRestModelArrayFactory<T> implements IRestModelFactory<T> {
	@Override
	public boolean isArray() {
		return true;
	}

	@Override
	public boolean isObject() {
		return false;
	}

	@Override
	public T fromJSON(JSONObject json) {
		return null;
	}
}
