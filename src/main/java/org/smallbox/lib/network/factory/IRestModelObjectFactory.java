package org.smallbox.lib.network.factory;

import org.json.JSONArray;

public abstract class IRestModelObjectFactory<T> implements IRestModelFactory<T> {
	@Override
	public boolean isArray() {
		return false;
	}

	@Override
	public boolean isObject() {
		return true;
	}
	
	@Override
	public T fromJSON(JSONArray array) {
		return null;
	}
}
