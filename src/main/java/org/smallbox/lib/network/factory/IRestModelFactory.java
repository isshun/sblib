package org.smallbox.lib.network.factory;

import org.json.JSONArray;
import org.json.JSONObject;

public interface IRestModelFactory<T> {
	T fromJSON(JSONObject json);
	T fromJSON(JSONArray array);
	boolean isArray();
	boolean isObject();
}
