package org.smallbox.lib.network.listener;

/**
 * Created by Alex on 08/12/2014.
 */
public interface ActionListener {
    void onReceive(String response);
    void onReadCache(String response);
    void onError();
}
