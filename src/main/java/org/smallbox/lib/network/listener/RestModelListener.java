package org.smallbox.lib.network.listener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.smallbox.lib.network.factory.IRestModelFactory;

public abstract class RestModelListener<T> implements ActionListener {

    private final Class mFactoryClass;

    public RestModelListener(Class factoryClass) {
        mFactoryClass = factoryClass;
    }

    @Override
    public void onReadCache(String message) {
        try {
            IRestModelFactory factory = (IRestModelFactory)mFactoryClass.newInstance();
            if (factory.isArray()) { onReadCache((T) factory.fromJSON(new JSONArray(message))); }
            else { onReadCache((T) factory.fromJSON(new JSONObject(message))); }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReceive(String message) {
        try {
            IRestModelFactory factory = (IRestModelFactory)mFactoryClass.newInstance();
            if (factory.isArray()) { onReceive((T) factory.fromJSON(new JSONArray(message))); }
            else { onReceive((T) factory.fromJSON(new JSONObject(message))); }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

	public abstract void onReceive(T data);
	public abstract void onReadCache(T data);
    public abstract void onError();
}
