package org.smallbox.lib.network;

/**
 * Created by Alex on 26/12/2014.
 */
public abstract class RSyncRequest {
    private String mFileName;
    private String mJson;
    private boolean mIsRunning;

    public void setFileName(String fileName) {
        mFileName = fileName;
    }
    public void setJson(String json) {
        mJson = json;
    }

    public String getFileName() {
        return mFileName;
    }
    public String getJson() {
        return mJson;
    }

    public abstract String getLabel();

    public void setRunning(boolean isRunning) {
        mIsRunning = isRunning;
    }

    public boolean isRunning() {
        return mIsRunning;
    }
}
