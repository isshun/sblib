package org.smallbox.lib.network.request;

import android.content.ContextWrapper;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;

import org.smallbox.lib.SBApplication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class PostRequest extends Request<String> {
	private static final String PROTOCOL_CHARSET = "utf-8";
	private static final String PROTOCOL_CONTENT_TYPE = String.format("application/json; charset=%s", PROTOCOL_CHARSET);

	private Listener<String> 	mListener;
	private String				mRequestBody;
	private ErrorListener 		mErrorListener;

	public PostRequest(String url, String body, Listener<String> listener, ErrorListener errorListener) {
		super(Method.POST, url, errorListener);

		mErrorListener = errorListener;
		mListener = listener;
		mRequestBody = body;
	}

	@Override
	public byte[] getBody() {
		try {
			return mRequestBody == null ? null : mRequestBody.getBytes(PROTOCOL_CHARSET);
		} catch (UnsupportedEncodingException uee) {
			VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, PROTOCOL_CHARSET);
			return null;
		}
	}

	@Override
	protected void deliverResponse(String response) {
		mListener.onResponse(response);
	}

	@Override
	public void deliverError(VolleyError error) {
		if (error.networkResponse != null && error.networkResponse.data != null) {
			try {
				ContextWrapper c = new ContextWrapper(SBApplication.getContext());
				FileOutputStream fos = new FileOutputStream(new File(c.getFilesDir(), "error_" + System.currentTimeMillis() + ".html"));
				fos.write(error.networkResponse.data);
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (error.getCause() != null && error.getCause() instanceof TimeoutError) {
			Log.e("RSyncService", "[RSyncService] TimeOut");
		}
		mErrorListener.onErrorResponse(error);
	}

	@Override
	public String getBodyContentType() {
		return PROTOCOL_CONTENT_TYPE;
	}

	@Override
	protected Response<String> parseNetworkResponse(NetworkResponse response) {
		String parsed;
		try {
			parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
		} catch (UnsupportedEncodingException e) {
			parsed = new String(response.data);
		}

		return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
	}

}
