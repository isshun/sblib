package org.smallbox.lib.network.request;

import android.content.ContextWrapper;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;

import org.smallbox.lib.SBApplication;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import ch.boye.httpclientandroidlib.HttpEntity;
import ch.boye.httpclientandroidlib.entity.mime.HttpMultipartMode;
import ch.boye.httpclientandroidlib.entity.mime.MultipartEntityBuilder;
import ch.boye.httpclientandroidlib.entity.mime.content.FileBody;

public class PostImageRequest extends Request<String> {
	private Listener<String> 	mListener;
	private ErrorListener 		mErrorListener;
	private HttpEntity 			mEntity;


	public PostImageRequest(String url, File file, Listener<String> listener, ErrorListener errorListener) {
		super(Method.POST, url, errorListener);

		mErrorListener = errorListener;
		mListener = listener;
        
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();        
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.addPart("profile_picture", new FileBody(file));
        mEntity = builder.build();
	}
	
    @Override
    public String getBodyContentType() {
        return mEntity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mEntity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }
    
	@Override
	protected void deliverResponse(String response) {
		mListener.onResponse(response);
	}

	@Override
	public void deliverError(VolleyError error) {
		if (error.networkResponse != null && error.networkResponse.data != null) {
			try {
				ContextWrapper c = new ContextWrapper(SBApplication.getContext());
				FileOutputStream fos = new FileOutputStream(new File(c.getFilesDir(), "error_" + System.currentTimeMillis() + ".html"));
				fos.write(error.networkResponse.data);
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (error.getCause() != null && error.getCause() instanceof TimeoutError) {
			Log.e("RSyncService", "[RSyncService] TimeOut");
		}
		mErrorListener.onErrorResponse(error);
	}

	@Override
	protected Response<String> parseNetworkResponse(NetworkResponse response) {
		String parsed;
		try {
			parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
		} catch (UnsupportedEncodingException e) {
			parsed = new String(response.data);
		}

		return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
	}

}
