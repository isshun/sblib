package org.smallbox.lib.network.route;

import com.android.volley.VolleyError;

import org.smallbox.lib.RouteManager;

/**
 * Created by Alex on 21/04/2015.
 */
public abstract class SBRoute {
    protected RouteManager    mManager;

    public String			url;

    public SBRoute(String url) {
        this.url = url;
    }

    public abstract void onLoad(String fileName, String json);
    public abstract void onSuccess(String response);
    public abstract void onPrepare();
    public abstract void onError(VolleyError error);

    public void setManager(RouteManager manager) {
        mManager = manager;
    }
}
