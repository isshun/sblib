package org.smallbox.lib.network.route;

import com.android.volley.VolleyError;

import org.smallbox.lib.RouteManager;

/**
 * Created by Alex on 21/04/2015.
 */
public class SBRouteSimple extends SBRoute {
    public SBRouteSimple(String url) { super(url); }

    @Override
    public void onLoad(final String fileName, String json) {
        mManager.addSync(fileName, json);
    }

    @Override
    public void onSuccess(String response) {}

    @Override
    public void onPrepare() {}

    @Override
    public void onError(VolleyError error) {}
}
