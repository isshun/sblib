package org.smallbox.lib;

import android.os.Handler;
import android.os.Looper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alex on 08/12/2014.
 */
public class EventManager {
    private static EventManager                 mSelf;
    private Map<Event, List<OnEventObserver>>  mListeners;
    private final Handler                       sHandler = new Handler(Looper.getMainLooper());

    public enum Event {
        RECEIVE_PROFILE,
        RECEIVE_PLAYERS,
        RECEIVE_PACKAGES,
        RECEIVE_UPDATE,
        RECEIVE_RESULTS,
        RECEIVE_CATEGORIES,
        RSYNC_UNABLE_TO_SEND,
        RSYNC_IS_CLEAR,
        RSYNC_HAS_REQUEST_WAITING,
        ADD_ANSWER,
        ADD_QUIZ_COMPLETE,
        DATA_SOURCES_LOADED,
        PACK_ACHIEVEMENT_COMPLETE,
        PACKAGE_DOWNLOAD,
        PACKAGE_INSTALL,
        ALL_PACKAGE_DOWNLOAD_COMPLETE,
        PACKAGE_DOWNLOAD_COMPLETE,
        UNLINK_ACCOUNT
    }

    public EventManager() {
        mListeners = new HashMap<>();
    }

    public static interface OnEventObserver {
        void onEvent(Event event, Object data);
    }

    public static interface OnEventListener {
        void onEvent(boolean isLive);
        void onFailure(String message);
    }

    public static EventManager getInstance() {
        if (mSelf == null) {
            mSelf = new EventManager();
        }
        return mSelf;
    }

    public void send(final Event event) {
        send(event, null);
    }

    public void send(final Event event, final Object data) {
        final List<OnEventObserver> eventListeners = mListeners.get(event);
        if (eventListeners != null) {
            sHandler.post(new Runnable() {
                @Override
                public void run() {
                    for (OnEventObserver listener: new ArrayList<>(eventListeners)) {
                        listener.onEvent(event, data);
                    }
                }
            });
        }
    }

    public void register(OnEventObserver listener, Event event) {
        List<OnEventObserver> eventListeners = mListeners.get(event);
        if (eventListeners == null) {
            eventListeners = new ArrayList<>();
            mListeners.put(event, eventListeners);
        }

        // Add listener
        eventListeners.add(listener);
    }

    public void release(OnEventObserver listener) {
        for (List<OnEventObserver> listeners: mListeners.values()) {
            listeners.remove(listener);
        }
    }

}
