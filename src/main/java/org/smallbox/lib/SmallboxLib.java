package org.smallbox.lib;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

public class SmallboxLib {
	private static Context sContext;

    public static Context getContext() {
		return sContext;
	}
	
	public static void setContext(Context context) {
		sContext = context;
	}
}
