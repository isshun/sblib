package org.smallbox.lib;

import android.content.ContextWrapper;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.smallbox.lib.network.RSyncRequest;
import org.smallbox.lib.network.RestAction;
import org.smallbox.lib.network.request.PostRequest;
import org.smallbox.lib.network.route.SBRoute;
import org.smallbox.lib.utils.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alex on 21/04/2015.
 */
public class RouteManager {
    protected static final long             TIME_BETWEEN_RETRY = 20000;

    private Map<String, SBRoute>            mUrls;
    protected Map<String, RestAction<?>>    mUrlActions;
    private List<RSyncRequest>              mRequests;
    private RequestQueue                    mRequestQueue;
    private Map<String, Request<?>>         mRequestWaiting;
    protected File                          mRSyncDirectory;
    protected long                          mLastError;

    public RouteManager() {
        mUrls = new HashMap<>();
        mUrlActions = new HashMap<>();
        mRequests = new ArrayList<>();
        mRequestWaiting = new HashMap<>();
    }

    void init(RequestQueue requestQueue) {
        mRequestQueue = requestQueue;
        ContextWrapper c = new ContextWrapper(SBApplication.getContext());
        mRSyncDirectory = (new File(c.getFilesDir(), "rsync/"));
        mRSyncDirectory.mkdir();

        // Load waiting files
        for (File file: mRSyncDirectory.listFiles()) {
            final String json = FileUtils.read(file);
            final String routeName = file.getName().substring(0, file.getName().indexOf('.'));
            SBRoute route = mUrls.get(routeName);
            route.onLoad(file.getName(), json);

            Log.i("Rsync", "Load rsync request from disk: " + file.getName());
        }

        sync();
    }

    public void addRoute(String url, SBRoute route) {
        route.setManager(this);
        mUrls.put(url, route);
    }

    public void addRoute(String url, RestAction action) {
        action.setManager(this);
        mUrlActions.put(url, action);
    }

    public void addSync(final String fileName, String json) {
        addSync(fileName, json, new RSyncRequest() {
            @Override
            public String getLabel() {
                return fileName;
            }
        });
    }

    public void addSync(final String fileName, String json, final String label) {
        addSync(fileName, json, new RSyncRequest() {
            @Override
            public String getLabel() {
                return label;
            }
        });
    }

    public void addSync(final String fileName, String json, RSyncRequest model) {
        model.setFileName(fileName);
        model.setJson(json);
        mRequests.add(model);
        EventManager.getInstance().send(EventManager.Event.RSYNC_HAS_REQUEST_WAITING);

        // Write file
        try {
            FileOutputStream fos = new FileOutputStream(new File(mRSyncDirectory, fileName));
            fos.write(json.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        sync();
    }

    public void sync() {
        sync(false);
    }

    public void forceSync() {
        sync(true);
    }

    private void sync(boolean forceSync) {
        if (forceSync == false && mLastError + TIME_BETWEEN_RETRY > System.currentTimeMillis()) {
            return;
        }

        if (mRequests.isEmpty()) {
            Log.i("RSync", "Is sync");
            return;
        }

        for (final RSyncRequest request: mRequests) {
            if (!request.isRunning()) {
                request.setRunning(true);
                final String routeName = request.getFileName().substring(0, request.getFileName().indexOf('.'));
                final SBRoute route = mUrls.get(routeName);
                if (route.url != null) {
                    Log.i("RSync", "Send json '" + request.getFileName() + "' to '" + route.url + "'");
                    Log.d("RSync", "Body:" + request.getJson());
                    PostRequest req = new PostRequest(route.url, request.getJson(), new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i("RSync", "Sync success: " + request.getFileName());
                            removeRequest(request);
                            route.onSuccess(response);
                            if (mRequests.isEmpty()) {
                                EventManager.getInstance().send(EventManager.Event.RSYNC_IS_CLEAR);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            request.setRunning(false);
                            mLastError = System.currentTimeMillis();
                            Log.i("RSync", "Unable to send " + request.getFileName() + " to " + route.url);
                            Log.e("RSync", "Error message is: " + error.getMessage());
                            route.onError(error);
                            EventManager.getInstance().send(EventManager.Event.RSYNC_UNABLE_TO_SEND);
                        }
                    });

                    // add the request object to the queue to be executed
                    addToQueue(req, true);
                }
            }
        }
    }

    public void addToQueue(Request<?> req, boolean fromRSync) {
        req.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // If RSync has file to send
        if (!fromRSync && hasRequestsWaiting()) {
            try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] a = req.getUrl().getBytes("UTF-8");
                byte[] b = req.getBody();
                byte[] buffer = null;
                if (b != null) {
                    buffer = new byte[a.length + b.length];
                    System.arraycopy(a, 0, buffer, 0, a.length);
                    System.arraycopy(b, 0, buffer, a.length, b.length);
                } else {
                    buffer = a;
                }

                mRequestWaiting.put(new String(md.digest(buffer)), req);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (AuthFailureError authFailureError) {
                authFailureError.printStackTrace();
            }

            sync();
        }

        // Or execute request
        else {
            mRequestQueue.add(req);
        }
    }

    public void executeWaiting() {
        for (Request<?> request: mRequestWaiting.values()) {
            mRequestQueue.add(request);
        }
    }

    protected void removeRequest(RSyncRequest request) {
        File file = new File(mRSyncDirectory, request.getFileName());
        if (file.exists()) {
            file.delete();
        }

        mRequests.remove(request);
        if (mRequests.isEmpty()) {
            executeWaiting();
        }
    }

    public File[] getFiles() {
        return mRSyncDirectory.listFiles();
    }

    public boolean hasRequestsWaiting() {
        return !mRequests.isEmpty();
    }

//    /**
//     * Update profile with waiting requests
//     *
//     * @param profile
//     */
//    public void update(ProfileModel profile) {
//        for (RSyncRequest request: mRequests) {
//            request.onUpdate(profile);
//        }
//    }
//
//    /**
//     * Update stats with waiting requests
//     *
//     * @param stats
//     */
//    public void update(ProfileStatsModel stats) {
//        for (RSyncRequest request: mRequests) {
//            request.onUpdate(stats);
//        }
//    }

    public List<RSyncRequest> getRequests() {
        return mRequests;
    }









    ////////////////////////////// REST /////////////////////////////

    public void cancel(String actionName) {
        RestAction<?> route = mUrlActions.get(actionName);
        if (route != null) {
            route.cancel();
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public RestAction execute(String actionName, EventManager.OnEventListener listener) {
        RestAction<?> route = mUrlActions.get(actionName);
        if (route != null) {
            if (route.isRunning() == false) {
                route.execute(listener);
                return route;
            }
        }
        return null;
    }

    public RestAction execute(String actionName) {
        return execute(actionName, null);
    }

    public void release(EventManager.OnEventListener listener) {
        for (RestAction action: mUrlActions.values()) {
            if (action.getListener() == listener) {
                action.removeListener();
            }
        }
    }
}
