package org.smallbox.lib.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import org.smallbox.lib.EventManager;
import org.smallbox.lib.RouteManager;
import org.smallbox.lib.SBApplication;

/**
 * Created by Alex on 21/04/2015.
 */
public abstract class SBActivity extends FragmentActivity implements EventManager.OnEventObserver {
    protected RouteManager mRouteManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRouteManager = SBApplication.getRouteManager();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mRouteManager = SBApplication.getRouteManager();
    }

    @Override
    protected void onPause() {
        super.onPause();

        EventManager.getInstance().release(this);
    }


}
