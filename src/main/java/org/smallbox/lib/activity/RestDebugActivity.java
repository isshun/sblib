package org.smallbox.lib.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.webkit.WebView;

/**
 * Created by Alex on 28/02/2015.
 */
public class RestDebugActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WebView webView = new WebView(this);
        webView.loadDataWithBaseURL("", getIntent().getStringExtra("data"), "text/html", "UTF-8", "");
        setContentView(webView);
    }
}
