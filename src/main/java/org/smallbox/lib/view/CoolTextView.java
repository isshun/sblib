package org.smallbox.lib.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import org.smallbox.lib.SBApplication;

public class CoolTextView extends TextView {
	private OnSizeChangeListener 	mListener;

	public CoolTextView(Context context) {
		super(context);
		init(null);
	}

	public CoolTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public CoolTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}

	private void init(AttributeSet attrs) {
		if (isInEditMode()) {
			return;
		}
		
		if (attrs != null) {
			setFont(attrs.getAttributeValue(null, "font"));
		} else if (SBApplication.getFontManager().hasDefaultFont()) {
			setTypeface(SBApplication.getFontManager().getDefaultFont());
		}
	}

	@Override
	protected void onLayout (boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		
		if (mListener != null) {
			mListener.onSizeChange(changed, right - left, bottom - top);
		}
	}
	
	public void setOnSizeChangeListener(OnSizeChangeListener listener) {
		mListener = listener;
	}

    protected void setFontIndex(int index) {
        Typeface typeface = SBApplication.getFontManager().getFont(index);
        if (typeface != null) {
            setTypeface(typeface);
        } else if (SBApplication.getFontManager().hasDefaultFont()) {
            setTypeface(SBApplication.getFontManager().getDefaultFont());
        }
    }

	public void setFont(String name) {
        Typeface typeface = SBApplication.getFontManager().getFont(name);
        if (typeface != null) {
            setTypeface(typeface);
        } else if (SBApplication.getFontManager().hasDefaultFont()) {
            setTypeface(SBApplication.getFontManager().getDefaultFont());
        }
	}
//
//    @Override
//    protected void onDraw(Canvas canvas) {
//        Paint strokePaint = new Paint();
//        strokePaint.setARGB(255, 0, 0, 0);
//        strokePaint.setTextAlign(Paint.Align.LEFT);
//        strokePaint.setTextSize(getTextSize());
//        strokePaint.setTypeface(getTypeface());
//        strokePaint.setStyle(Paint.Style.STROKE);
//        strokePaint.setStrokeWidth(2);
//
//        Paint textPaint = new Paint();
//        textPaint.setARGB(255, 255, 255, 255);
//        textPaint.setTextAlign(Paint.Align.LEFT);
//        textPaint.setTextSize(getTextSize());
//        textPaint.setTypeface(getTypeface());
//
//        canvas.drawText(getText().toString(), 100, 100, strokePaint);
//        //canvas.drawText("Some Text", 100, 100, textPaint);
//
//        super.onDraw(canvas);
//    }

}
