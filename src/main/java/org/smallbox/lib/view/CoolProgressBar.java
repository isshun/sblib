package org.smallbox.lib.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import org.smallbox.lib.utils.Utils;

public class CoolProgressBar extends View {
    private int PADDING;
    private int MAX_SIZE;
    private int MIN_SIZE;
    private int mLoop;
    private int mIndex = 0;
    private float mSize[];
    private int mOffset[];
    private Paint mPaint;
    private boolean mIsSmall;

    public CoolProgressBar(Context context, boolean isSmall) {
        super(context);
        init(isSmall);
    }

    public CoolProgressBar(Context context) {
        super(context);
        init(false);
    }

    public CoolProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
        init(false);
	}

    private void init(boolean isSmall) {
        if (!isInEditMode()) {
            mIsSmall = isSmall;
            PADDING = Utils.convertDpToPixel(isSmall ? 2 : 4);
            MAX_SIZE = Utils.convertDpToPixel(isSmall ? 10 : 18);
            MIN_SIZE = Utils.convertDpToPixel(isSmall ? 4 : 12);
            mSize = new float[] {MIN_SIZE, MIN_SIZE, MIN_SIZE};
            mOffset = new int[] {1, 1, 1};

            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setStrokeWidth(2);
            mPaint.setColor(0xffffffff);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (!isInEditMode()) {
            if (mSize[mIndex] > MAX_SIZE) { mOffset[mIndex] = -1; }
            if (mSize[mIndex] < MIN_SIZE) { mOffset[mIndex] = 1; mIndex = (mIndex + 1) % 3; }

            if (mIsSmall) mSize[mIndex] += mOffset[mIndex] / 2f;
            else mSize[mIndex] += mOffset[mIndex];

            for (int i = 0; i < 3; i++) {
                int offset = (MAX_SIZE + PADDING) * i;
                RectF rectF = new RectF(offset + MAX_SIZE - mSize[i], MAX_SIZE - mSize[i], offset + mSize[i], mSize[i]);
                canvas.drawOval(rectF, mPaint);
            }

            invalidate();
        }
    }
}
