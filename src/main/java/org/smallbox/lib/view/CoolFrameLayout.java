package org.smallbox.lib.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class CoolFrameLayout extends FrameLayout {
	private OnSizeChangeListener 	mListener;

	public CoolFrameLayout(Context context) {
		super(context);
	}

	public CoolFrameLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CoolFrameLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onLayout (boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		
		if (mListener != null) {
			mListener.onSizeChange(changed, right - left, bottom - top);
		}
	}
	
	public void setOnMeasureListener(OnSizeChangeListener listener) {
		mListener = listener;
	}

}
