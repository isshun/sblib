package org.smallbox.lib.view;

public interface OnSizeChangeListener {
	void onSizeChange(boolean changed, int width, int height);
}
